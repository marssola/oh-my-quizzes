import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ToolBar {
    id: headerBar
    property string title
    property string subTitle

    height: 50

    ColumnLayout {
        anchors.fill: parent

        Label {
            text: headerBar.title
            font.bold: true

            Layout.fillHeight: true
            Layout.fillWidth: true
            horizontalAlignment: Label.AlignHCenter
            verticalAlignment: Label.AlignVCenter
            font.pixelSize: 18
            wrapMode: Label.WordWrap
            elide: Label.ElideRight
        }

        Label {
            visible: headerBar.subTitle
            text: headerBar.subTitle
            Layout.fillWidth: true
            Layout.preferredHeight: 20
            wrapMode: Label.WordWrap
            elide: Label.ElideRight
            horizontalAlignment: Label.AlignHCenter
            verticalAlignment: Label.AlignVCenter
        }
    }
}
