import QtQuick 2.12
import QtQuick.Controls 2.12
import "qrc:/components"

NavigationPage {
    id: page
    objectName: "pages/SettingsPage"

    pane.height: window.height - window.header.height - window.footer.height
    property var headerComponent: loaderHeaderDefault.item
    property var footerComponent: loaderFooterDefault.item
    onHeaderComponentChanged: headerComponent.title = qsTr("Settings")

    content: Column {
        Item {
            width: parent.width
            height: 10
        }

        GroupItems {
            width: parent.width
            title: qsTr("Theme")

            Column {
                width: parent.width

                SwitchDelegate {
                    width: parent.width
                    text: qsTr("Dark")
                    checked: conf.dark
                    onCheckedChanged: conf.dark = checked
                }
            }
        }
    }
}


