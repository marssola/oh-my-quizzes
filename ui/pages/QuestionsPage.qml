import QtQuick 2.12
import QtQuick.Controls 2.12
import "qrc:/components"

NavigationPage {
    id: page
    objectName:"pages/QuestionsPage"

    property var headerComponent: loaderHeaderDefault.item
    property var footerComponent: loaderFooterDefault.item
    onHeaderComponentChanged: headerComponent.title = qsTr("Questions")

    content: Item {
    }
}
