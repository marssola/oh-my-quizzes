import QtQuick 2.12
import QtQuick.Controls 2.12
import Qt.labs.settings 1.1
import UiKit 1.0
import "qrc:/components"

ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("Oh My Quizzes")

    Settings {
        id: conf
        property bool dark: false
        onDarkChanged: window.UiKit.dark = dark
    }

    ListModel {
        id: listmodel

        property string page: ""

        ListElement {
            name: qsTr("Quiz")
            iconName: "question-answer"
            page: "pages/QuizPage"
        }

        ListElement {
            name: qsTr("Questions")
            iconName: "playlist-add"
            page: "pages/QuestionsPage"
        }

        ListElement {
            name: qsTr("Settings")
            iconName: "settings"
            page: "pages/SettingsPage"
        }
    }

    StackView {
       id: stackview
       anchors.fill: parent
       prefix_file: "qrc:/ui/"
       suffix_file: ".qml"

       initialItem: prefix_file + listmodel.get(0).page + suffix_file

       onCurrentItemChanged: {
            if (currentItem.footerComponent)
                window.footer = currentItem.footerComponent
            window.header = currentItem.headerComponent
       }
    }

    Loader {
        id:loaderHeaderDefault
        source: "qrc:/ui/navigation/HeaderBar.qml"
        visible: false

        onLoaded: window.header = item
    }

    Loader {
        id: loaderFooterDefault
        visible: false

        onLoaded: window.footer = item
        Component.onCompleted: setSource("qrc:/ui/navigation/FooterBar.qml", {model: listmodel})
    }
}

